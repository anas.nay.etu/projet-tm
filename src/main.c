#include "../includes/frequence.h"
#include "../includes/gestion_mot.h"
#include "../includes/utils.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

int principale() {
  int choix = 0;

  // Boucle jusqu'à ce que l'utilisateur fournisse une option valide
  while (choix != 1 && choix != 2 && choix != 3) {
    printf("Choisissez une option :\n");
    printf("1. Compter les fréquences de mots d'un fichier texte\n");
    printf("2. Compter les n-grams d'un fichier texte\n");
    printf("3. Mode interactif\n");
    printf("Votre choix : ");
    scanf("%d", &choix);

    if (choix != 1 && choix != 2 && choix != 3) {
      printf("Option invalide. Veuillez choisir une option valide.\n");
    }
  }

  switch (choix) {
  case 1: {
    char fichierEntree[100];
    char fichierSortie[100];
    printf("Entrez le nom du fichier d'entrée : ");
    scanf("%s", fichierEntree);
    printf("Entrez le nom du fichier de sortie pour les mots : ");
    scanf("%s", fichierSortie);

    FILE *fichierEntreePointer = fopen(fichierEntree, "r");
    if (!fichierEntreePointer) {
      fprintf(stderr, "Erreur lors de l'ouverture du fichier d'entrée.\n");
      exit(EXIT_FAILURE);
    }

    FILE *fichierSortiePointer = fopen(fichierSortie, "w");
    if (!fichierSortiePointer) {
      fprintf(
          stderr,
          "Erreur lors de l'ouverture du fichier de sortie pour les mots.\n");
      fclose(fichierEntreePointer);
      exit(EXIT_FAILURE);
    }

    FrequenceMot *freqMots = malloc(MAX_MOTS * sizeof(FrequenceMot));
    if (freqMots == NULL) {
      fprintf(stderr, "Erreur d'allocation de mémoire pour freqMots.\n");
      fclose(fichierEntreePointer);
      fclose(fichierSortiePointer);
      exit(EXIT_FAILURE);
    }

    int nbMots = 0;
    char line[MAX_LONGUEUR_MOT];
    int lignesTraitees = 0;
    int totalLignes = 0;

    while (fgets(line, sizeof(line), fichierEntreePointer) != NULL) {
      totalLignes++;
    }

    rewind(fichierEntreePointer);

    while (fgets(line, sizeof(line), fichierEntreePointer) != NULL) {
      char mot[MAX_LONGUEUR_MOT];
      int i = 0, j = 0;

      while (line[i]) {
        if (isalpha(line[i])) {
          mot[j++] = tolower(line[i]);
        } else if (j > 0) {
          mot[j] = '\0';
          if (!isStopWord(mot)) {
            ajouterMot(mot, freqMots, &nbMots);
          }
          j = 0;
        }
        i++;
      }

      // if (j > 0) {
      //     mot[j] = '\0';
      //     if (!isStopWord(mot)) {
      //         ajouterMot(mot, freqMots, &nbMots);
      //     }
      // }

      lignesTraitees++;
      float pourcentage = (float)lignesTraitees / totalLignes * 100;
      afficherPourcentage(pourcentage);
    }

    qsort(freqMots, nbMots, sizeof(FrequenceMot), comparer);

    for (int i = 0; i < nbMots; i++) {
      if (freqMots[i].frequence > 0) {
        fprintf(fichierSortiePointer, "%s: %d\n", freqMots[i].mot,
                freqMots[i].frequence);
      }
    }

    free(freqMots);
    fclose(fichierEntreePointer);
    fclose(fichierSortiePointer);
    break;
  }

  case 2: {
    char fichierEntree[100];
    char fichierSortie[100];
    printf("Entrez le nom du fichier d'entrée : ");
    scanf("%s", fichierEntree);
    printf("Entrez le nom du fichier de sortie pour les n-grams : ");
    scanf("%s", fichierSortie);

    FILE *fichierEntreePointer = fopen(fichierEntree, "r");
    if (!fichierEntreePointer) {
      fprintf(stderr, "Erreur lors de l'ouverture du fichier d'entrée.\n");
      exit(EXIT_FAILURE);
    }

    FILE *fichierSortiePointer = fopen(fichierSortie, "w");
    if (!fichierSortiePointer) {
      fprintf(stderr, "Erreur lors de l'ouverture du fichier de sortie pour "
                      "les n-grams.\n");
      fclose(fichierEntreePointer);
      exit(EXIT_FAILURE);
    }

    int choixNgram;
    printf("Choisissez le nombre de mots par N-gram (par exemple, 2 pour les "
           "2-grams) : ");
    scanf("%d", &choixNgram);

    if (choixNgram < 1) {
      fprintf(
          stderr,
          "Le nombre de mots par N-gram doit être supérieur ou égal à 1.\n");
      exit(EXIT_FAILURE);
    }

    processNGrams(fichierEntreePointer, fichierSortiePointer, choixNgram);

    fclose(fichierEntreePointer);
    fclose(fichierSortiePointer);
    break;
  }

  case 3: {
    char texte[MAX_TAILLE_TEXTE];
    printf("Entrez le texte : ");
    scanf(" %[^\n]s", texte); // Lecture du texte jusqu'à la nouvelle ligne

    // Consommer la nouvelle ligne restante
    getchar();

    FrequenceMot *freqMots = malloc(MAX_MOTS * sizeof(FrequenceMot));
    if (freqMots == NULL) {
      fprintf(stderr, "Erreur d'allocation de mémoire pour freqMots.\n");
      exit(EXIT_FAILURE);
    }

    int nbMots = 0;
    compterMots(texte, freqMots, &nbMots);

    // Trier les mots par fréquence décroissante
    qsort(freqMots, nbMots, sizeof(FrequenceMot), comparer);

    // Afficher les résultats
    printf("Fréquence des mots :\n");
    for (int i = 0; i < nbMots; i++) {
      if (freqMots[i].frequence > 0) {
        printf("%s: %d\n", freqMots[i].mot, freqMots[i].frequence);
      }
    }

    free(freqMots);

    break;
  }

  default:
    printf("Option invalide.\n");
    exit(EXIT_FAILURE);
  }

  printf("Le traitement s'est effectué avec succès!\n");

  return 0;
}
