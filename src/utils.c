// Fichier : utils.c

#include "../includes/frequence.h"
#include "../includes/utils.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

void enleverPonctuationEtMinuscule(char *mot) {
  // Convertir en minuscules
  for (int i = 0; mot[i]; i++) {
    mot[i] = tolower(mot[i]);
  }

  // Supprimer la ponctuation
  char *dst = mot;
  for (char *src = mot; *src; src++) {
    if (isalpha(*src) || *src == ' ') { // Ne supprimer que si c'est un
                                        // caractère alphabétique ou un espace
      *dst++ = *src;
    }
  }
  *dst = '\0'; // Terminer la chaîne correctement
}

int isStopWord(char *word) {
  const char *stopWords[] = {
      "i",          "me",        "my",      "myself", "we",         "our",
      "ours",       "ourselves", "you",     "your",   "yours",      "yourself",
      "yourselves", "he",        "him",     "his",    "himself",    "she",
      "her",        "hers",      "herself", "it",     "its",        "itself",
      "they",       "them",      "their",   "theirs", "themselves", "what",
      "which",      "who",       "whom",    "this",   "that",       "these",
      "those",      "am",        "is",      "are",    "was",        "were",
      "be",         "been",      "being",   "have",   "has",        "had",
      "having",     "do",        "d",       "does",   "did",        "doing",
      "a",          "an",        "the",     "and",    "but",        "if",
      "or",         "because",   "as",      "until",  "while",      "of",
      "at",         "by",        "for",     "with",   "about",      "against",
      "between",    "into",      "through", "during", "before",     "after",
      "above",      "below",     "to",      "from",   "up",         "down",
      "in",         "out",       "on",      "off",    "over",       "under",
      "again",      "further",   "then",    "once",   "here",       "there",
      "when",       "where",     "why",     "how",    "all",        "any",
      "both",       "each",      "few",     "more",   "most",       "other",
      "some",       "such",      "no",      "nor",    "not",        "only",
      "own",        "same",      "so",      "than",   "too",        "very",
      "s",          "t",         "can",     "will",   "just",       "don",
  };

  int numStopWords = sizeof(stopWords) / sizeof(stopWords[0]);
  for (int i = 0; i < numStopWords; i++) {
    if (strcmp(word, stopWords[i]) == 0) {
      return 1;
    }
  }

  return 0;
}

void afficherPourcentage(float pourcentage) {
  printf("\rAvancement : %.2f%%", pourcentage);
  fflush(stdout); // Forcer le vidage du tampon de sortie pour afficher
                  // immédiatement la mise à jour
  if (pourcentage == 100.0) {
    printf("\n"); // Saut de ligne après l'affichage du pourcentage à 100%
  }
}

void cleanLine(char *line) {
  int i, j;
  int length = strlen(line);

  // Suppression des espaces en début de ligne
  while (isspace(line[0])) {
    for (i = 0; i < length; i++) {
      line[i] = line[i + 1];
    }
    length--;
  }

  // Suppression des espaces en fin de ligne
  while (isspace(line[length - 1])) {
    line[length - 1] = '\0';
    length--;
  }

  // Suppression des espaces en double
  for (i = 0; i < length; i++) {
    if (isspace(line[i]) && isspace(line[i + 1])) {
      for (j = i; j < length; j++) {
        line[j] = line[j + 1];
      }
      length--;
      i--;
    }
  }
}

// Fonction pour rechercher la fréquence d'un N-gramme dans un tableau
int getFrequency(FrequenceMot *ngrams, int *freq, char *ngram, int numNgrams) {
  for (int i = 0; i < numNgrams; i++) {
    if (strcmp(ngrams[i].mot, ngram) == 0) {
      return freq[i];
    }
  }
  return 0;
}
