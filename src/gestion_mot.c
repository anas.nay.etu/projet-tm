#include "../includes/gestion_mot.h"
#include "../includes/utils.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Compter les fréquences de mots
int comparer(const void *a, const void *b) {
  const FrequenceMot *wordA = (const FrequenceMot *)a;
  const FrequenceMot *wordB = (const FrequenceMot *)b;

  if (wordA->frequence < wordB->frequence) {
    return 1;
  } else if (wordA->frequence > wordB->frequence) {
    return -1;
  } else {
    return strcmp(wordA->mot, wordB->mot);
  }
}

void ajouterMot(char *mot, FrequenceMot freqMots[], int *nbMots) {
  int i;

  // Vérifier si le mot est déjà dans la liste des mots fréquents
  for (i = 0; i < *nbMots; i++) {
    if (strcmp(freqMots[i].mot, mot) == 0) {
      // Si le mot est déjà présent, incrémenter sa fréquence et quitter la
      // fonction
      freqMots[i].frequence++;
      return;
    }
  }

  // Si le mot n'est pas encore présent, l'ajouter à la liste
  if (*nbMots < MAX_MOTS) {
    strcpy(freqMots[*nbMots].mot, mot);
    freqMots[*nbMots].frequence = 1;
    (*nbMots)++;
  } else {
    fprintf(
        stderr,
        "Nombre maximal de mots atteint. Impossible d'ajouter le mot : %s\n",
        mot);
  }
}

void processNGrams(FILE *file, FILE *outputFile, int n) {
  char line[MAX_LONGUEUR_MOT];

  FrequenceMot *ngrams = malloc(MAX_NGRAMS * sizeof(FrequenceMot));
  int *freq = malloc(MAX_NGRAMS * sizeof(int));
  int numNgrams = 0;

  if (ngrams == NULL || freq == NULL) {
    fprintf(stderr, "Erreur d'allocation de mémoire.\n");
    exit(EXIT_FAILURE);
  }

  while (fgets(line, sizeof(line), file) != NULL) {
    cleanLine(line);
    enleverPonctuationEtMinuscule(line);

    char *tokens[MAX_NGRAMS];
    int numTokens = 0;
    char *token = strtok(line, " ");
    while (token != NULL) {
      tokens[numTokens++] = token;
      token = strtok(NULL, " ");
    }

    for (int i = 0; i < numTokens - n + 1; i++) {
      char ngram[MAX_LONGUEUR_MOT * n];
      strcpy(ngram, tokens[i]);
      for (int j = 1; j < n; j++) {
        strcat(ngram, " ");
        strcat(ngram, tokens[i + j]);
      }
      int found = 0;
      for (int k = 0; k < numNgrams; k++) {
        if (strcmp(ngrams[k].mot, ngram) == 0) {
          freq[k]++;
          found = 1;
          break;
        }
      }
      if (!found) {
        strcpy(ngrams[numNgrams].mot, ngram);
        freq[numNgrams] = 1;
        numNgrams++;
      }
    }
  }

  for (int i = 0; i < numNgrams; i++) {
    int frequency = getFrequency(ngrams, freq, ngrams[i].mot, numNgrams);
    fprintf(outputFile, "%s: %d\n", ngrams[i].mot, frequency);
  }

  qsort(ngrams, numNgrams, sizeof(FrequenceMot), compareNgrams);

  free(ngrams);
  free(freq);
}

// Fonction de comparaison pour qsort
int compareNgrams(const void *a, const void *b) {
  const FrequenceMot *ngramA = (const FrequenceMot *)a;
  const FrequenceMot *ngramB = (const FrequenceMot *)b;

  // Tri par ordre décroissant de fréquence
  return ngramB->frequence - ngramA->frequence;
}

// Mode interactif
void compterMots(const char *texte, FrequenceMot *freqMots, int *nbMots) {
  // Initialiser le tableau de fréquences des mots
  *nbMots = 0;

  // Parcourir le texte pour compter les mots
  char mot[MAX_LONGUEUR_MOT] = "";
  int index_mot = 0;
  int index_texte = 0;

  while (texte[index_texte] != '\0') {
    // Si le caractère actuel est une lettre, l'ajouter au mot
    if (isalpha(texte[index_texte])) {
      mot[index_mot++] = tolower(texte[index_texte]);
    }
    // Sinon, si un mot est en cours de formation
    else if (index_mot > 0) {
      mot[index_mot] = '\0'; // Terminer le mot
      index_mot = 0;         // Réinitialiser l'indice du mot

      // Vérifier si le mot est un stopword
      if (!isStopWord(mot)) {
        // Vérifier si le mot existe déjà dans le tableau de fréquences
        int i;
        for (i = 0; i < *nbMots; i++) {
          if (strcmp(freqMots[i].mot, mot) == 0) {
            freqMots[i].frequence++; // Incrémenter la fréquence du mot
            break;
          }
        }
        // Si le mot n'existe pas, l'ajouter au tableau de fréquences
        if (i == *nbMots) {
          strcpy(freqMots[i].mot, mot);
          freqMots[i].frequence = 1;
          (*nbMots)++;
        }
      }
    }

    index_texte++;
  }

  // Traitement du dernier mot si le texte se termine par un mot
  if (index_mot > 0) {
    mot[index_mot] = '\0';
    if (!isStopWord(mot)) {
      int i;
      for (i = 0; i < *nbMots; i++) {
        if (strcmp(freqMots[i].mot, mot) == 0) {
          freqMots[i].frequence++;
          break;
        }
      }
      if (i == *nbMots) {
        strcpy(freqMots[i].mot, mot);
        freqMots[i].frequence = 1;
        (*nbMots)++;
      }
    }
  }
}
