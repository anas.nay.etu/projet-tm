// Fichier : utils.h

#ifndef UTILS_H
#define UTILS_H

void enleverPonctuationEtMinuscule(char *mot);
int isStopWord(char *word);
void afficherPourcentage(float pourcentage);
void cleanLine(char *line);
int getFrequency(FrequenceMot *ngrams, int *freq, char *ngram, int numNgrams);

#endif // UTILS_H
