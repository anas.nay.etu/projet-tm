#ifndef FREQUENCE_H
#define FREQUENCE_H

#define MAX_LONGUEUR_MOT 512
#define MAX_MOTS 30000
#define MAX_MOTS_VIDES 1000
#define MAX_NGRAMS 10000
#define MAX_TAILLE_TEXTE 1000

typedef struct {
  char mot[MAX_LONGUEUR_MOT];
  int frequence;
} FrequenceMot;

#endif
