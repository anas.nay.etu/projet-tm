// Fichier : gestion_mot.h

#ifndef GESTION_MOT_H
#define GESTION_MOT_H

#include "frequence.h"
#include "utils.h"
#include <stdio.h>

int comparer(const void *a, const void *b);
void ajouterMot(char *mot, FrequenceMot freqMots[], int *nbMots);
void processNGrams(FILE *file, FILE *outputFile, int n);
int compareNgrams(const void *a, const void *b);
void compterMots(const char *texte, FrequenceMot *freqMots, int *nbMots);

#endif // GESTION_MOT_H
