# projet-TM

Ce projet vise à développer un compteur de fréquence de mots dans un texte donné. Il comprend à la fois le code principal et les tests unitaires pour assurer la qualité du logiciel.

## Contenu du dépôt

- `README.md` le fichier que vous être en train de lire
- `Makefile` permet la compilation du code métier (cible `main`) et des tests unitaires (cible `tests`)
- le répertoire `includes` contient les déclarations de fonctions
- le répertoire `src` contient les sources du projet en particulier
    + `utils.c`: qui contient des fonctions auxiliaires utiles pour les autres fichiers `.c`
    + `gestion_mot.c` : contient les fonctions nécessaires au tri des mots, à la soustraction des stop-words, ... 
    + `main.c`: constitue le coeur du programme, offrant à l'utilisateur la possibilité d'interagir avec le logiciel pour compter les fréquences de mots dans un fichier texte, générer des n-grams ou utiliser le mode interactif pour saisir du texte directement dans la console.
    + `launch.c` : permet de lancer la fonction principale.
- le répertoire `tests` contient le fichier `.c` qui permet de réaliser les tests unitaires.
- le répertoire `couverture` contient les fichiers nécessaires pour générer un rapport de couverture de code.

## Compilation

Pour compiler le code principal, utilisez la commande suivante :

```
make main
```

Pour compiler les tests unitaires, utilisez la commande :

```
make tests
```

Exécution

Pour exécuter le programme principal, utilisez la commande :

```
make run_main
```

Pour exécuter les tests unitaires, utilisez la commande :

```
make run_tests
```

Nettoyage

Pour nettoyer les fichiers objets et les fichiers générés, utilisez la commande :

```
make clean
```

Pour supprimer complètement les fichiers générés, utilisez la commande :

```
make mrproper
```
Couverture de code

Pour générer un rapport de couverture de code, utilisez la commande :

```
make coverage
```

Avant de lancer la commande, assurez-vous d'avoir executer les 3 versions du programme principal.

Le rapport de couverture sera disponible dans le répertoire **couverture/coverage_report**.

## Auteurs

Le code a été écrit par Anas Nay et Antoine Leloir

