/**
 * @brief      This file implements unit tests for sorting algorithms
 *
 * @author     Antoine/Anas
 * @date       2024
 */
#include "../includes/gestion_mot.h"
#include <check.h>
#include <frequence.h>
#include <stdlib.h>
#include <utils.h>

START_TEST(test_comparer) {

  FrequenceMot wordA = {"apple", 5};
  FrequenceMot wordB = {"banana", 3};

  int result = comparer(&wordA, &wordB);

  ck_assert_int_eq(result, -1);
}

END_TEST

START_TEST(test_enlever_ponctuation_et_minuscule) {
  char mot1[] = "Hello, World!/";
  enleverPonctuationEtMinuscule(mot1);
  ck_assert_str_eq(mot1, "hello world");

  char mot2[] = "How are you?:";
  enleverPonctuationEtMinuscule(mot2);
  ck_assert_str_eq(mot2, "how are you");
}

END_TEST

START_TEST(test_is_stop_word) {
  // Test avec un mot qui est un stop word
  ck_assert_int_eq(isStopWord("the"), 1);

  // Test avec un mot qui n'est pas un stop word
  ck_assert_int_eq(isStopWord("hello"), 0);
}

END_TEST

START_TEST(test_clean_line) {
  char line1[] = "  hello world  ";
  char line2[] = "  hello  world  ";
  char line3[] = "hello   world";

  cleanLine(line1);
  cleanLine(line2);
  cleanLine(line3);

  ck_assert_str_eq(line1, "hello world");
  ck_assert_str_eq(line2, "hello world");
  ck_assert_str_eq(line3, "hello world");
}

END_TEST

START_TEST(test_get_frequency_present) {
  // Définir quelques n-grams et fréquences
  FrequenceMot ngrams[] = {{"hello", 0}, {"world", 1}, {"foo", 2}};
  int freq[] = {10, 20, 30};
  int numNgrams = sizeof(ngrams) / sizeof(ngrams[0]);

  // Tester avec un n-gram présent
  int frequency = getFrequency(ngrams, freq, "world", numNgrams);
  ck_assert_int_eq(frequency, 20);
}

START_TEST(test_get_frequency_not_present) {
  // Définir quelques n-grams et fréquences
  FrequenceMot ngrams[] = {{"hello", 0}, {"world", 1}, {"foo", 2}};
  int freq[] = {10, 20, 30};
  int numNgrams = sizeof(ngrams) / sizeof(ngrams[0]);

  // Tester avec un n-gram non présent
  int frequency = getFrequency(ngrams, freq, "bar", numNgrams);
  ck_assert_int_eq(frequency, 0);
}

END_TEST

START_TEST(test_ajouter_mot_present) {
  FrequenceMot freqMots[10]; // Suppose que MAX_MOTS est défini à 10
  int nbMots = 0;

  // Ajouter un mot existant
  ajouterMot("hello", freqMots, &nbMots);
  ajouterMot("hello", freqMots, &nbMots);

  // Vérifier que la fréquence du mot a été incrémentée
  ck_assert_int_eq(freqMots[0].frequence, 2);
}

START_TEST(test_ajouter_mot_not_present) {
  FrequenceMot freqMots[10]; // Suppose que MAX_MOTS est défini à 10
  int nbMots = 0;

  // Ajouter un nouveau mot
  ajouterMot("world", freqMots, &nbMots);

  // Vérifier que le mot a été ajouté avec une fréquence de 1
  ck_assert_int_eq(freqMots[0].frequence, 1);
}

END_TEST

/*START_TEST(test_process_ngrams) {
  // Créer un fichier d'entrée temporaire avec des données
  FILE *inputFile = fopen("test_input.txt", "w");
  fprintf(inputFile, "This is a test input file for processNGrams function\n");
  fclose(inputFile);

  // Créer un fichier de sortie temporaire
  FILE *outputFile = fopen("test_output.txt", "w");

  // Appeler la fonction à tester
  inputFile = fopen("test_input.txt", "r");
  processNGrams(inputFile, outputFile, 2);
  fclose(inputFile);
  fclose(outputFile);

  // Vérifier le contenu du fichier de sortie
  FILE *resultFile = fopen("test_output.txt", "r");
  char line[100];
  int lines = 0;
  while (fgets(line, sizeof(line), resultFile) != NULL) {
    lines++;
  }
  fclose(resultFile);

  // Vérifier que le fichier de sortie contient le bon nombre de lignes
  ck_assert_int_eq(lines, 9); // Mettez le nombre de n-grams attendu ici

  // Supprimer les fichiers temporaires
  remove("test_input.txt");
  remove("test_output.txt");
}

END_TEST*/

START_TEST(test_compareNgrams) {
  FrequenceMot ngramA = {"apple", 5};
  FrequenceMot ngramB = {"banana", 3};

  int result = compareNgrams(&ngramA, &ngramB);

  ck_assert_int_lt(result, 0); // Vérifie si ngramA est plus grand que ngramB
                               // selon l'ordre décroissant
}

END_TEST

/*START_TEST(test_compterMots) {
  const char *texte = "Le chat est sur le toit. Le toit est rouge.";

  FrequenceMot freqMots[MAX_MOTS];
  int nbMots = 0;

  compterMots(texte, freqMots, &nbMots);

  // Vérifiez les fréquences des mots spécifiques dans le texte fourni
  int le_freq = 0, chat_freq = 0, sur_freq = 0, toit_freq = 0, est_freq = 0,
      rouge_freq = 0;
  for (int i = 0; i < nbMots; i++) {
    if (strcmp(freqMots[i].mot, "le") == 0) {
      le_freq = freqMots[i].frequence;
    } else if (strcmp(freqMots[i].mot, "chat") == 0) {
      chat_freq = freqMots[i].frequence;
    } else if (strcmp(freqMots[i].mot, "sur") == 0) {
      sur_freq = freqMots[i].frequence;
    } else if (strcmp(freqMots[i].mot, "toit") == 0) {
      toit_freq = freqMots[i].frequence;
    } else if (strcmp(freqMots[i].mot, "est") == 0) {
      est_freq = freqMots[i].frequence;
    } else if (strcmp(freqMots[i].mot, "rouge") == 0) {
      rouge_freq = freqMots[i].frequence;
    }
  }

  ck_assert_int_eq(le_freq, 3);
  ck_assert_int_eq(chat_freq, 1);
  ck_assert_int_eq(sur_freq, 1);
  ck_assert_int_eq(toit_freq, 2);
  ck_assert_int_eq(est_freq, 2);
  ck_assert_int_eq(rouge_freq, 1);
}
END_TEST*/

Suite *freq_suite(void) {
  Suite *s = suite_create("SortAlgorithms");
  TCase *tc_core = tcase_create("Core");
  tcase_add_test(tc_core, test_comparer);
  tcase_add_test(tc_core, test_enlever_ponctuation_et_minuscule);
  tcase_add_test(tc_core, test_is_stop_word);
  tcase_add_test(tc_core, test_clean_line);
  tcase_add_test(tc_core, test_get_frequency_present);
  tcase_add_test(tc_core, test_get_frequency_not_present);
  tcase_add_test(tc_core, test_ajouter_mot_present);
  tcase_add_test(tc_core, test_ajouter_mot_not_present);
  // tcase_add_test(tc_core, test_process_ngrams);
  tcase_add_test(tc_core, test_compareNgrams);
  // tcase_add_test(tc_core, test_compterMots);
  suite_add_tcase(s, tc_core);

  return s;
}

int main(void) {
  int no_failed = 0;
  Suite *s = freq_suite();
  SRunner *runner = srunner_create(s);
  srunner_run_all(runner, CK_NORMAL);
  no_failed = srunner_ntests_failed(runner);
  srunner_free(runner);
  return (no_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
