CC=gcc
CFLAGS=-W -Wall -g -Iincludes/ -I/home/a/n/anay/TPTM/projet/projet-tm/build/ -I/home/a/n/anay/TPTM/projet/projet-tm/build/
EXTRA_CFLAGS=-fprofile-arcs -ftest-coverage
EXTRA_LDFLAGS=-L/home/a/n/anay/TPTM/projet/projet-tm/build/ --coverage

#### Variables liées à la compilation des sources
OBJ= build/main.o build/utils.o build/gestion_mot.o
OBJ2=build/launch.o
EXEC=build/compteur_freq
DIR= couverture

#### Variables liées à la compilation des tests unitaires
TEST_SRC=tests/unit_tests.c
TEST_OBJ=build/unit_tests.o
TEST_EXEC=build/freq_tests
TEST_LDFLAGS=-L/home/a/n/anay/TPTM/projet/projet-tm/build/  -lcheck -lpthread -lm -lrt -lsubunit
TEST_OS_DEP_LFLAGS=

UNAME := $(shell uname)

all: main

########### Compilation du code principal ###########
main: $(EXEC)

$(EXEC): $(OBJ) $(OBJ2)
	$(CC) $^ $(EXTRA_LDFLAGS) -o $@

build/main.o: src/main.c includes/gestion_mot.h includes/utils.h includes/frequence.h
	$(CC) $(CFLAGS) -c $< -o $@

build/utils.o: src/utils.c includes/utils.h includes/frequence.h
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) -c $< -o $@

build/gestion_mot.o: src/gestion_mot.c includes/gestion_mot.h includes/frequence.h includes/utils.h
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) -c $< -o $@
	
build/launch.o: src/launch.c includes/main.h
	$(CC) $(CFLAGS) $(EXTRA_CFLAGS) -c $< -o $@
	
########### Compilation du code de test ###########
tests: $(TEST_EXEC)

$(TEST_EXEC): $(TEST_OBJ) $(OBJ)
	$(CC) $(OBJ) $(TEST_OBJ) $(TEST_LDFLAGS) $(EXTRA_LDFLAGS) $(TEST_OS_DEP_LFLAGS) -o $(TEST_EXEC)

$(TEST_OBJ): $(TEST_SRC)
	$(CC) $(CFLAGS) -c $(TEST_SRC) -o $(TEST_OBJ)
	
#################### Exécution #####################
# Cible pour exécuter le programme principal
run_main: $(EXEC)
	./$(EXEC)

# Cible pour exécuter les tests unitaires
run_tests: $(TEST_EXEC)
	./$(TEST_EXEC)

#################### Nettoyage #####################
clean:
	@rm -rf build/*.o build/*.gc* *.gcov

mrproper: clean


	@rm -rf $(EXEC) build/launch
	
#################### Coverage #####################

coverage:
	gcov -f -b $(OBJ2)
	lcov --directory build --base-directory . -c -o $(DIR)/cov.info
	genhtml $(DIR)/cov.info -o $(DIR)/coverage_report



